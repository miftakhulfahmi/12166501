<header class="masthead text-center text-white d-flex">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold">Selamat Datang DI Pelatihan WEB Programming II</h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          <p class="text-white-75 font-weight-light mb-5"> ~ Miftakhul Fahmi ~ "Sebagus-bagusnya Program pasti ada yang ga suka, Sejelek-jeleknya Program pasti ada aja orang yang suka. Jadi mau Program bagus atau jelek jangan pernah takut berkarya"</p>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>
        </div>
      </div>
    </div>
  </header>